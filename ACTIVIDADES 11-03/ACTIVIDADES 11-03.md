# os-subject
1. ¿Qué es un repositorio?
Almacén o lugar donde se guardan ciertas cosas
2. ¿Qué es una rama?
Son utilizadas para desarrollar funcionalidades aisladas unas de otras.
3. ¿Son origin y local dos repositorios?
Si
4. ¿Qué es .gitignore?
Sirve para decirle a git los archivos que tiene que ignorar
5. Busca 5 ejemplos de .gitignore en repositorios de gitlab
No los encuentro
6. Describe los siguiente comandos
- Status: Muestra el estado del directorio de trabajo y el área de preparación
- Add: Para agregar archivos
- Commit: Se usa para guardar los cambios en el repositorio local
- Clone: Para clonar repositorios
- Push: Envia cambios a la rama principal
- Pull: Fusionar todos los cambios
- Checkout: Crear ramas o cambiar entre ellas
- Branch: Para crear o borrar ramas
- Log: Muestra una lista de commit
- Merge: Combinar una rama
7. Explica git add -u
Realiza modificaciones y eliminaciones, sin archivos nuevos
